function addElement() {

    var item_input = document.forms['todo_form']['item']
    var item = item_input.value.trim();

    var list = document.getElementById('item-list');

    if(item.length > 0) {
        var node = document.createElement("li");
        node.className += "list-group-item";
        var textnode = document.createTextNode(item);
        node.appendChild(textnode);
        list.appendChild(node);
        item_input.value = "";
    }

    return false;
}
